# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from ChallengeApp.models import *
import subprocess
import requests
import urllib
import sys
import os

def home(request):
	thankyou = request.GET.get('thankyou', None) is not None
	return HttpResponse(get_template('index.html').render({'thankyou' : thankyou}, request))

def about(request):
	return HttpResponse(get_template('about.html').render({}, request))

def contact(request):
	return HttpResponse(get_template('contact.html').render({}, request))

@csrf_exempt
def send_message(request):
	return HttpResponse('OK')

@csrf_exempt
def run(request):
	ip = request.POST['ip'].decode('base64')
	cmd = 'ping -c 1 "{ip}"'.format(ip=ip)
	result = subprocess.Popen(executable='runuser', args=['runuser', '-l', 'aweakuser', '-c', cmd], stdout=subprocess.PIPE).communicate()[0]
	return HttpResponse(result)

