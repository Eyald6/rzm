function run() {
	$('.output-div').hide()
	if (/^[0-9\.]+$/gim.test(ip.value)) {
		$.post('/check', {ip : btoa(ip.value)}).done(function(data) {
			if (data.length > 0) {
				$('.output-div').show()
				output.innerHTML = data;
			}
		})
	}
}

function send_message() {
	if (text.value.length > 0) {
		$.post('/send_message', {text : text.value}).done(function(data) {
			window.location = '/?thankyou'
		})
	} else {
		showError('Please enter some text')
	}
}

function showError(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#error-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function showNotification(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#notification-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
