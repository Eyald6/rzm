USERNAME_MD5 = "5d03fd9683a87341286428fb557a59f2"
PASSWORD_MD5 = "2039d1e9a406ec5fda2da7f924aef156"

function showError(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#error-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function showNotification(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#notification-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function run() {
	cmd64 = btoa(cmd_input.value)
	$.get('/run?cmd=' + cmd64).done(function(data) {
		output.innerHTML = data
	})
}

function connect() {
	if (md5(username.value) != USERNAME_MD5) {
		showError('Wrong Username!')
	} else if (md5(password.value) != PASSWORD_MD5) {
		showError('Wrong Password!')
	} else {
		$.post("/auth", {username : username.value, password : md5(password.value)}).done(function(data) {
			window.location = data
		})
	}
}