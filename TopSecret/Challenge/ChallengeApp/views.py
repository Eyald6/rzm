# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from ChallengeApp.models import *
from django.db import connection
cursor = connection.cursor()
from threading import Lock

db_lock = Lock()

def documents(request):
    documents = Documents.objects.all()
    return HttpResponse(get_template('index.html').render({'documents' : documents}, request))

def get_document(request):
    try:
        document_id = request.GET['id']
        soldier_id = 1
        query = "SELECT CASE WHEN classification > (SELECT classification FROM ChallengeApp_documents WHERE id = {document_id}) "\
                 "then (SELECT GROUP_CONCAT(document_text, '\n --- \n') FROM ChallengeApp_documents WHERE id = {document_id}) "\
                 "else 'Your Classification: ' || classification || '\n' || (SELECT 'Classification Requred: ' || classification FROM ChallengeApp_documents WHERE id = {document_id}) end FROM ChallengeApp_soldiers WHERE id = {soldier_id}".format(soldier_id=soldier_id, document_id=document_id)
        with db_lock:
            cursor.execute(query)
            document = cursor.fetchall()[0][0]
        return HttpResponse(get_template('document.html').render({'document' : document}, request))
    except:
        return HttpResponse('ERROR')