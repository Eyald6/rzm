# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import re
import json

# Create your models here.
def my_save(self, *args, **kwargs):
    """
    Before creating the object, run object.clean.
    """
    clean_function = getattr(self, 'clean', None)
    if clean_function is not None:
        clean_function()
    # Run the original save.
    self._save(*args, **kwargs)

# Whenever an object is saved - we will verify it's validity.
models.Model._save = models.Model.save
models.Model.save = my_save

#For example: class MyModel(models.Model):
class Documents(models.Model):
    document_name = models.CharField(max_length=0xffffffff, blank=False)
    classification = models.IntegerField(blank=False)
    document_text = models.CharField(max_length=0xffffffff, blank=False)
    
    def clean(self):
        assert len(self.document_text) > 0

class Soldiers(models.Model):
    classification = models.IntegerField(blank=False)