function connect() {
	$.post("/auth", {username : username.value, password : password.value}).done(function(data) {
		window.location = data
	})
}

function register() {
	$.post("/register", {username : username.value, password : password.value}).done(function(data) {
		window.location = data
	})
}

function add_note() {
	$.post('/add_note', {text : newnote.value}).done(function(data) {
		if (data != 'OK') {
			alert(data)
		} else {
			window.location.reload()
		}
	})
}

function logout() {
  document.cookie = 'user=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  window.location.reload()
}