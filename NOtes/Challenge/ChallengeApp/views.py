# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from ChallengeApp.models import *


def validate_cookie(func):
    def wrapper(request):
        try:
            user = User.objects.get(name=request.COOKIES['user'].decode('base64'))
        except:
            return login(request)
        return func(request, user)
    return wrapper


@validate_cookie
def home(request, user):
    return HttpResponse(get_template('index.html').render({'notes' : user.notes}, request))

def login(request):
    failed = request.GET.get('fail', None) is not None
    exists = request.GET.get('exists', None) is not None
    return HttpResponse(get_template('login.html').render({'failed' : failed, 'exists' : exists}, request))

@csrf_exempt
def register(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    if not User.exists(username):
        User(name=username, password=password).save()
        response = HttpResponse('/')
        response.set_cookie('user', username.encode('base64').strip())
        return response
    else:
        return HttpResponse('/?exists')

@csrf_exempt
def auth(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    if User.objects.filter(name=username, password=password).count() > 0:
        response = HttpResponse('/')
        response.set_cookie('user', username.encode('base64').strip())
        return response
    else:
        return HttpResponse('/?fail')

@csrf_exempt
@validate_cookie
def add_note(request, user):
    try:
        if user.name in ['admin', 'pElegantit']:
            return HttpResponse('Disabled')
        user.add_note(request.POST['text'])
        return HttpResponse('OK')
    except:
        return HttpResponse('Error')