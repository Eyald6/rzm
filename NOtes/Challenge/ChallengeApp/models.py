# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import re
import json

# Create your models here.
def my_save(self, *args, **kwargs):
    """
    Before creating the object, run object.clean.
    """
    clean_function = getattr(self, 'clean', None)
    if clean_function is not None:
        clean_function()
    # Run the original save.
    self._save(*args, **kwargs)

# Whenever an object is saved - we will verify it's validity.
models.Model._save = models.Model.save
models.Model.save = my_save

#For example: class MyModel(models.Model):
class User(models.Model):
    name = models.CharField(max_length=0xffffffff, blank=False, unique=True)
    password = models.CharField(max_length=0xffffffff, blank=False)
    user_notes = models.CharField(max_length=0xffffffff, default='[]')

    def clean(self):
        try:
            json.loads(self.user_notes)
        except:
            self.user_notes = json.dumps([])
            self.save()
        assert len(self.password) > 0
        assert len(self.name) > 0

    @property
    def notes(self):
        return json.loads(self.user_notes)
    
    def add_note(self, text):
        notes = self.notes
        notes.append(text)
        self.user_notes = json.dumps(notes)
        self.save()

    @staticmethod
    def exists(name):
        return User.objects.filter(name=name).count() > 0