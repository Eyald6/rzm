<html>
 <head>
  <title>Football World</title>
  <script src="/static/js/jquery-3.2.1.js"></script>
  <script src="/static/js/material.min.js"></script>
  <script src="/static/js/app.js"></script>

  <link rel="stylesheet" href="/static/css/material.brown-light_green.min.css" />
  <link rel="stylesheet" href="/static/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/static/css/google.fonts.css">
  <link rel="stylesheet" href="/static/css/app.css">
 </head>
 <body>
 	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
 	  <header class="mdl-layout__header portfolio-header">
 	              <div class="mdl-layout__header-row portfolio-logo-row">
 	                  <span class="mdl-layout__title">
 	                      <a href="/"><img class="portfolio-logo" src="/static/imgs/logo.png"></a>
 	                  </span>
 	              </div>
 	              <div class="mdl-layout__header-row portfolio-navigation-row">
 	                  <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
 	                    <a class="mdl-navigation__link" href="/teams/manchester_united">Manchester United</a>
 	                    <a class="mdl-navigation__link" href="/teams/manchester_city">Manchester City</a>
 	                    <a class="mdl-navigation__link" href="/teams/chelsea">Chelsea</a>
 	                    <a class="mdl-navigation__link" href="/teams/real_madrid">Real Madrid</a>
 	                    <script>
 	                      $('a[href="' + window.location.pathname + '"]').toggleClass('is-active')
 	                    </script>
 	                  </nav>
 	              </div>
 	  </header>
 	  <main class="mdl-layout__content">
 	    <div class="inner-container" style="overflow: auto;">
    	 	<div align="center" class="container">
				<?php
					   set_include_path('/var/www/html/teams/');
					   if ( isset( $_GET['team']) && strlen($_GET['team']) > 0 ) {
    	 				  echo "<div class=\"team-div\">";
					      include( $_GET['team'] . '.php' );
    	 				  echo "</div>";
					   } else if ( isset( $_GET['team']) ) {
					   		echo "<h4 class=\"error\">Error: No 'team' parameter was provided in the get request</h4>";
					   } else {
					   		echo "<div class='welcome-div'>
					   			<h5>Hello and welcome to FootballWorld!</h5>
					   			Here we will keep you updated on the lineups of the 4 teams we currently track:
					   			<ul>
					   				<li>Manchester United</li>
					   				<li>Manchester City</li>
					   				<li>Chelsea</li>
					   				<li>Real Madrid</li>
				   				</ul>
					   			More teams to come in the near future!
					   		</div>";
					   }
				?>
    	 	</div>
 	    </div>
 	  </main>
 	</div>

 </body>
</html>
