# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from ChallengeApp.models import *


def join(request):
    reqid = request.COOKIES.get('reqid', None)
    has_request = reqid is not None
    is_finished = Request.objects.filter(id=reqid).count() == 0
    return HttpResponse(get_template('join.html').render({
        'has_request' : has_request,
        'is_finished' : is_finished
    }, request))

@csrf_exempt
def join_request(request):
    try:
        r = Request(text=request.POST['text'])
        r.save()
        response = HttpResponse('OK')
        response.set_cookie('reqid', r.id)
        return response
    except:
        return HttpResponse('Error')

def admin(request):
    # Prevent anyone from accessing this page (Except phantomjs who knows the token)
    if request.GET.get('token', None) != 'IamTh3RealzZz4dmin!!!@':
        return HttpResponseRedirect('/')

    requests = [r.id for r in Request.objects.all()]
    return HttpResponse(get_template('index.html').render({'requests' : requests}, request))

def get_request(request):
    try:
        if request.GET.get('token', None) != 'IamTh3RealzZz4dmin!!!@':
            return HttpResponseRedirect('/')
        req = Request.objects.get(id=request.GET['id'])
        text = req.text
        # Clear the requests we are just visting.
        req.delete()
        return HttpResponse(text)
    except:
        return HttpResponse('Error')