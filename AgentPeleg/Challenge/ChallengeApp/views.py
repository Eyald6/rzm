# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from ChallengeApp.models import *
import md5
PELEG_PASSWORD = r"RZM{Sm0keWeedErrrr'yDay}"

def home(request):
    if request.user.is_authenticated:
        return HttpResponse(get_template('index.html').render({}, request))
    else:
        return login(request)

def login(request):
    failed = request.GET.get('fail', None) is not None
    return HttpResponse(get_template('login.html').render({'failed' : failed}, request))

@csrf_exempt
def auth(request):
    username = request.POST.get('username', None)
    password_md5 = request.POST.get('password', None)
    password = None
    if password_md5 == md5.md5(PELEG_PASSWORD).hexdigest():
        password = PELEG_PASSWORD
    user = authenticate(request=request, username=username, password=password)
    if user is not None:
        django_login(request, user)
        return HttpResponse('/')
    else:
        return HttpResponse('/?fail')

