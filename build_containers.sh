echo -n "Building AgentPeleg . . ."
cd ./AgentPeleg;
docker build -t python:AgentPeleg  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building FootballWorld . . ."
cd ./FootballWorld;
docker build -t php:FootballWorld  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building FootballWorld2 . . ."
cd ./FootballWorld2;
docker build -t php:FootballWorld2  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building InternetSafe . . ."
cd ./InternetSafe;
docker build -t python:InternetSafe  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building NOtes . . ."
cd ./NOtes;
docker build -t python:NOtes  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building OutLaws . . ."
cd ./OutLaws;
docker build -t python:OutLaws  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building SirYesSir . . ."
cd ./SirYesSir;
docker build -t python:SirYesSir  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building SirYesSir2 . . ."
cd ./SirYesSir2;
docker build -t python:SirYesSir2  . --no-cache > /dev/null
echo "Done"
cd ../;

echo -n "Building TopSecret . . ."
cd ./TopSecret;
docker build -t python:TopSecret  . --no-cache > /dev/null
echo "Done"
cd ../;
