"""CTFPlatform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views
import django.contrib.staticfiles.views

urlpatterns = [
    url(r'^login', views.login),
    url(r'^board', views.board),
    url(r'^admin', views.admin),
    url(r'^send_flag', views.send_flag),
    url(r'^create_cookie', views.create_cookie),
    url(r'^docs', views.docs),
    url(r'^challenges', views.home),
    url(r'^challenge_info', views.challenge_info),
    url(r'^ajax/get_tags', views.get_tags),
    url(r'^add_challenge', views.add_challenge_editor),
    url(r'^add_doc', views.add_doc_editor),
    url(r'^ajax/remove_challenge', views.remove_challenge),
    url(r'^ajax/filter_docs', views.filter_docs),
    url(r'^ajax/add_challenge', views.add_challenge),
    url(r'^ajax/add_doc', views.add_doc),
	url(r'.*', views.redirect_home),
]
urlpatterns += staticfiles_urlpatterns()