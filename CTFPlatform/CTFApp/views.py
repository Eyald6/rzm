# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login
from django.views.decorators.http import require_http_methods
from CTFApp.models import *
import requests
import datetime
import urllib
import sys
import os


@csrf_exempt
def login(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    user = authenticate(request=request, username=username, password=password)
    if user is not None:
        django_login(request, user)
        return redirect_home(request)
    else:
        return redirect('/admin?failed')


def admin(request):
	if request.user.is_authenticated:
		return redirect_home(request)
	context = {
		'failed' : 'failed' in request.GET.dict()
	}
	return HttpResponse(get_template('admin.html').render(context, request))

def redirect_home(request):
	return redirect('/challenges')


@csrf_exempt
def create_cookie(request):
	try:
		user_name = request.POST['name']
		assert len(user_name) > 1, 'Name invalid'
		User.objects.get(cookie=request.COOKIES.get('userid', None))
		return redirect_home(request)
	except ObjectDoesNotExist:
		user_cookie = os.urandom(128).encode('base64')
                max_age = 365 * 24 * 60 * 60
                expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
		new_user = User(name=user_name, cookie=user_cookie)
		new_user.save()
		response = HttpResponse('OK')
		response.set_cookie('userid', user_cookie, max_age=max_age, expires=expires)
		return response
	except:
		return HttpResponse('אופס! נתקלנו בשגיאה')


def validate_cookie(func):
	def wrapper(request):
		try:
			if request.user.is_authenticated:
				user = request.user
			else:
				user = User.objects.get(cookie=request.COOKIES['userid'])
		except:
			return HttpResponse(get_template('create_cookie.html').render({}, request))
		return func(request, user)
	return wrapper


@validate_cookie
def home(request, user):
	context = {
		'user' : user,
		'levels' : Challenge.get_by_levels()
	}
	return HttpResponse(get_template('challenges.html').render(context, request))


@validate_cookie
def docs(request, user):
	context = {
		'user' : user,
	}
	return HttpResponse(get_template('docs.html').render(context, request))


@csrf_exempt
@validate_cookie
def filter_docs(request, user):
	categories = []
	try:
		tags = json.loads(request.POST.get('tags', []))
		docs_by_category = {}
		if len(tags) > 0:
			docs_by_category = Doc.get_by_tags(tags)
		else:
			docs_by_category = Doc.get_all()

		for category in docs_by_category:
			categories.append({
				'name' : category,
				'docs' : [doc.get_objet() for doc in docs_by_category[category]]
			})
		return HttpResponse(json.dumps(categories))
	except:
		raise
		return HttpResponse(json.dumps(categories))

@validate_cookie
def challenge_info(request, user):
	try:
		challenge = Challenge.objects.get(id=request.GET['id'])
		context = { 
			'challenge' : challenge,
			'user' : user,
		}
		return HttpResponse(get_template('challenge_info.html').render(context, request))
	except:
		return redirect('/challenges')


@csrf_exempt
@validate_cookie
def send_flag(request, user):
	try:
		challenge = Challenge.objects.get(id=request.POST['id'])
		flag = request.POST['flag']
		if flag == challenge.challenge_solution:
			user.add_solved(challenge.id)
			return HttpResponse('OK')
		else:
			return HttpResponse('Mistake')
	except:
		return HttpResponse('Error')

@csrf_exempt
@login_required(login_url='/admin')
def add_challenge_editor(request):
	context = {}
	return HttpResponse(get_template('add_challenge.html').render(context, request))

@csrf_exempt
@require_http_methods(["POST"])
@login_required(login_url='/admin')
def add_challenge(request):
	try:
		c = Challenge(challenge_name=request.POST['challenge_name'],
			  challenge_level=request.POST['challenge_level'],
			  challenge_solution=request.POST['challenge_solution'],
			  challenge_description=request.POST['challenge_description'],
			  challenge_tags=request.POST['challenge_tags'],
			  challenge_link=request.POST['challenge_link'])
		c.save()
		return HttpResponse(c.id)
	except:
		return HttpResponse('ERROR')

@login_required(login_url='/admin')
def board(request):
	context = {
		'challenges' : Challenge.objects.all(),
		'users' : User.objects.all()
	}
	return HttpResponse(get_template('trackboard.html').render(context, request))

@csrf_exempt
@login_required(login_url='/admin')
def add_doc_editor(request):
	context = {}
	return HttpResponse(get_template('add_doc.html').render(context, request))

@csrf_exempt
@require_http_methods(["POST"])
@login_required(login_url='/admin')
def add_doc(request):
	try:
		d = Doc(name=request.POST['name'],
			  	doc_tags=request.POST['tags'],
			  	category=request.POST['category'],
			  	link=request.POST['link'])
		d.save()
		return HttpResponse(d.id)
	except:
		return HttpResponse('ERROR')

@csrf_exempt
@require_http_methods(["POST"])
@login_required(login_url='/admin')
def remove_challenge(request):
	try:
		c = Challenge.objects.get(id=request.POST['id'])
		c.delete()
		return HttpResponse('OK')
	except:
		return HttpResponse('ERROR')

@login_required(login_url='/admin')
def get_tags(request):
	try:
		prefix = request.GET['prefix']
		tags = json.dumps(Tag.get_tags(prefix))
		return HttpResponse(tags)
	except:
		return HttpResponse('ERROR')
