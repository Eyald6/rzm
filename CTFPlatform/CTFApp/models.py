# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import re
import json

# Create your models here.
def my_save(self, *args, **kwargs):
    """
    Before creating the object, run object.clean.
    """
    clean_function = getattr(self, 'clean', None)
    if clean_function is not None:
        clean_function()
    # Run the original save.
    self._save(*args, **kwargs)

# Whenever an object is saved - we will verify it's validity.
models.Model._save = models.Model.save
models.Model.save = my_save

class Tag(models.Model):
    name = models.CharField(max_length=120, blank=False)

    @staticmethod
    def get_tags(prefix):
        if len(prefix) > 0:
            return [tag.name for tag in Tag.objects.all() if tag.name.lower().startswith(prefix.lower())]
        return []

class Challenge(models.Model):
    challenge_name = models.CharField(max_length=120, blank=False)
    challenge_level = models.IntegerField(blank=False)
    challenge_solution = models.CharField(max_length=120, blank=False)
    challenge_description = models.CharField(max_length=0xffffffff, blank=False)
    challenge_tags = models.CharField(max_length=0xffffffff, blank=False)
    challenge_link = models.CharField(max_length=0xffffffff, blank=False)

    def clean(self):
        for tag in json.loads(self.challenge_tags):
            if Tag.objects.filter(name=tag).count() == 0:
                Tag(name=tag).save()
        if not re.match('RZM\{.*\}', self.challenge_solution):
            raise Exception('Challenge solution should look like RZM\{Secret Flag Here\}')

    @property
    def tags(self):
        try:
            return json.loads(self.challenge_tags)
        except:
            self.challenge_tags = json.dumps([])
            self.save()
            return self.tags

    @staticmethod
    def get_by_levels():
        levels = {}
        for challenge in Challenge.objects.all():
            if challenge.challenge_level not in levels:
                levels[challenge.challenge_level] = []
            levels[challenge.challenge_level].append(challenge)
        return levels

    def set_tags(self, tags):
        self.challenge_tags = json.dumps(tags)
        self.save()

    def add_tag(self, tag):
        self.set_tags(self.tags + [tag])
        self.save()

    def remvoe_tag(self, tag):
        tags = self.tags
        tags.remove(tag)
        self.set_tags(tags)
        self.save()

    def to_json(self):
        pass

class User(models.Model):
    cookie = models.CharField(max_length=128, blank=False)
    name = models.CharField(max_length=120, blank=False)
    solved_challenges = models.CharField(max_length=0xffffffff, default='[]')

    def clean(self):
        try:
            json.loads(self.solved_challenges)
        except:
            raise Exception('Could not parse solved_challenges list (Maybe its not a valid json?)')        

    def to_json(self):
        pass

    @property
    def solved(self):
        try:
            return json.loads(self.solved_challenges)
        except:
            self.solved_challenges = json.dumps([])
            self.save()
            return self.solved_challenges

    def add_solved(self, challenge_id):
        try:
            Challenge.objects.get(id=challenge_id)
            assert isinstance(challenge_id, int), "Challenge id must be an integer"
            new_solved = list(set(self.solved + [challenge_id]))
            self.solved_challenges = json.dumps(new_solved)
            self.save()
        except:
            raise Exception('Could not add challenge to solved list')
    
class Doc(models.Model):
    name = models.CharField(max_length=0xffffffff, blank=False)
    link = models.CharField(max_length=0xffffffff, blank=False)
    category = models.CharField(max_length=128, blank=False)
    doc_tags = models.CharField(max_length=0xffffffff, default='[]')
    
    @property
    def tags(self):
        try:
            return json.loads(self.doc_tags)
        except:
            self.doc_tags = json.dumps([])
            self.save()
            return self.tags

    def has_tag(self, tag):
        return tag.lower() in [t.lower() for t in self.tags]

    def clean(self):
        for tag in json.loads(self.doc_tags):
            if Tag.objects.filter(name=tag).count() == 0:
                Tag(name=tag).save()

    @staticmethod
    def get_by_tags(tags):
        """
        Returns docs by categories in the order of relevance (number of matching tags in each doc)
        """
        relevent_docs = {}
        for doc in Doc.objects.all():
            if any([doc.has_tag(tag) for tag in tags]):
                if doc.category not in relevent_docs:
                    relevent_docs[doc.category] = []
                relevent_docs[doc.category].append(doc)

        return relevent_docs

    @staticmethod
    def get_all():
        relevent_docs = {}
        for doc in Doc.objects.all():
            if doc.category not in relevent_docs:
                relevent_docs[doc.category] = []
            relevent_docs[doc.category].append(doc)
        return relevent_docs

    def get_objet(self):
        return {
            'name' : self.name,
            'tags' : self.tags,
            'link' : self.link,
        }