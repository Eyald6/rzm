function add_challenge() {
	tags = $('.tag').map( function() {
    	return $(this).attr('value');
	}).get();

	$.post('/ajax/add_challenge', {
		challenge_name : challenge_name.value,
		challenge_level : challenge_level.value,
		challenge_description : challenge_description.value,
		challenge_link : challenge_link.value,
		challenge_solution : 'RZM{' + challenge_solution.value + '}',
		challenge_tags : JSON.stringify(tags),
	}).done(function(data) {
		if (data == 'ERROR') {
			showError('Could not create challenge')
		} else {
			window.location = '/challenge_info?id=' + data
		}
	})
}

function add_doc() {
	tags = $('.tag').map( function() {
    	return $(this).attr('value');
	}).get();

	$.post('/ajax/add_doc', {
		name : doc_name.value,
		tags : JSON.stringify(tags),
		link : doc_link.value,
		category : doc_category.value,
	}).done(function(data) {
		if (data == 'ERROR') {
			showError('Could not create doc')
		} else {
			window.location = '/docs'
		}
	})
}

function remove_challenge(challenge_id) {
	if (confirm('Are you sure?')) {
		$.post('/ajax/remove_challenge', {id : challenge_id}).done(function(data) {
			if (data == 'OK') {
				window.location.reload();
			} else {
				showError('Could not delete challenge');
			}
		})
	}
}