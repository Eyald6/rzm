function create_cookie() {
	$.post('/create_cookie', {name : user_name.value}).done(function(data) {
		if (data == "OK") {
			window.location = '/'
			window.location.reload()
		} else if (data.length < 100) {
			showError(data)
		} else {
			window.location.reload()
		}
	})
}

function send_flag() {
	$.post('/send_flag', {flag : flag.value, id : challenge_id.value}).done(function(data) {
		if (data == "OK") {
			window.location.reload()
		} else if (data == "Mistake") {
			showError('טעות!')
		} else {
			showError('שגיאה!')
			setTimeout(function() {window.location.reload()}, 2500);
		}
	})
}

function showError(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#error-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function showNotification(message, timeout=2750) {
  var snackbarContainer = document.querySelector('#notification-toast');
  var data = {
              message: message,
              timeout: timeout,
            };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function censor(tag) {
	text_element = $(tag).find('.mdl-chip__text')[0]
	if ($(tag).hasClass('censored')) {
		text_element.innerHTML = text_element.getAttribute('value')
	} else {
		text_element.innerHTML = 'רמז'
	}
	$(tag).toggleClass('censored')
}

function autocomplete_tag() {
	$.get('/ajax/get_tags?prefix=' + tag.value).done(function(data) {
		$(tags_dropdown).empty()
		if (data != 'ERROR') {
			tags = JSON.parse(data)
			for (var i=0; i < tags.length; i++) {		
				option = document.createElement('li')
				$(option).click(function(event) {
					tag.value = event.target.innerHTML;
					$('.mdl-menu__container.is-upgraded').removeClass('is-visible')
					add_tag();
				})
				$(option).toggleClass('mdl-menu__item')
				option.innerHTML = tags[i]
				$(tags_dropdown).append(option)
			}
		}
		
		if ($(tags_dropdown.children).length > 0) {
			$('.mdl-menu__container.is-upgraded').addClass('is-visible')
		} else {
			$('.mdl-menu__container.is-upgraded').removeClass('is-visible')
		}
	})
}


function add_tag() {
	if ($('.tag[value="' + tag.value + '"]').length == 0 && tag.value.length > 0) {
		tag_element = `<span class="mdl-chip mdl-chip--contact" dir="ltr" style="margin: 3px;">
						    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">#</span>
						    <span class="mdl-chip__text tag" value="` + tag.value + `">` + tag.value + `</span>
						    <button type="button" class="mdl-chip__action" onclick="remove_tag('` + tag.value + `')"><i class="material-icons">cancel</i></button>
						</span>`
		$('#tags-div').append(tag_element)
		tag.value = "";
	}
	$('.mdl-menu__container.is-upgraded').removeClass('is-visible')
}

function remove_tag(value) {
	$('.tag[value="' + value + '"]').parent().remove()
}


function filter_docs() {
	tags = $('.tag').map( function() {
		return $(this).attr('value');
	}).get();
	$.post('/ajax/filter_docs', {tags : JSON.stringify(tags)}).done(function(data) {
		$('#docs-div').empty()
		categories = JSON.parse(data)
		docs_html = ''
		for (var a=0; a < categories.length; a++) {
			category = categories[a]
			docs = category.docs
			docs_html += `<h1 class="doc-category" align="center">` + category.name + `</h1>
				<ul class="mdl-list">`
			for (var b=0; b < docs.length; b++) {
				doc = docs[b]
				docs_html += `<li class="mdl-list__item mdl-list__item--three-line">
				    <span class="mdl-list__item-primary-content">
				      <i class="material-icons mdl-list__item-avatar">language</i>
				      <span>` + doc.name + `</span>
				      <span class="mdl-list__item-text-body">`
				for (var c=0; c < doc.tags.length; c++) {
					docs_html += doc.tags[c] + ", "
				}
				docs_html += `</span>
				    </span>
				    <span class="mdl-list__item-secondary-content">
				      <a class="mdl-list__item-secondary-action" href="` + doc.link + `"><i class="material-icons">link</i></a>
				    </span>
				  </li>`
			}
			docs_html += "</ul>"
		}
		$('#docs-div')[0].innerHTML = docs_html
	});
}