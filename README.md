## What is this?

That is a very lightweight platform for a simple web-driven CTF (Capture The Flag).
The platform was written without the competition theme in mind, and it's main purpose is teaching new-commers about web-exploits.

## Levels

The platform contains 10 different challenges, each is trying to demonstrate a different vulnerability.
It is split to different "stages", based on the difficulty level.

## Motivation

1. Teaching about the power of web-exploits.
2. FUN!

## Installation

1. To begin, install docker on your linux machine (apt install).
2. Clone this git repository anywhere you'd like.
3. CD inside the git repository (Where this file is placed).
4. run 'build_containers.sh'
5. run 'run_containers.sh'
6. run 'cd CTFPlatfom; python manage.py runserver 0.0.0.0:80' to run the server :)

That should be it...

## Solutions

Will be up soon.
Try it your self until then.

## Administration

Navigate to [mysite]/admin
- admin:RZM{AdminPassw0rd}

That should give you administrative tools like adding challenges, docs or seeing the scoreboard.

## Author

Ayul.
