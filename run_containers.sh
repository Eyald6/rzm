# Run the containers with exposed ports, -d for daemonizing.
echo Stopping all running dockers.
docker stop `docker ps -aq`
echo Running Dockers
docker run -dp 1337:80 python:AgentPeleg
docker run -dp 1338:80 python:InternetSafe
docker run -dp 1339:80 python:NOtes
docker run -dp 1340:80 python:OutLaws
docker run -dp 1341:80 python:SirYesSir
docker run -dp 1342:80 php:FootballWorld
docker run -dp 1343:80 php:FootballWorld2
docker run -dp 1344:80 python:SirYesSir2
docker run -dp 1345:80 python:TopSecret
echo Done